using System;
using System.Collections.Generic;
using System.Linq;

class ContaCorrente {
    private decimal saldo;
    private DateTime dataCriacao;
    private string titular;
    private int counter;
    private List<decimal> listaSaldo = new List<decimal>();

    public ContaCorrente(string nome, decimal val) {
        titular = nome;
        saldo = val;
        dataCriacao = DateTime.Now;
        counter = 1;
        listaSaldo.Add(val);
    }

    public decimal Saldo { 
        get { return saldo;}
    }
    public string Titular{
        get { return titular;}
    }
        
    public DateTime DataCriacao{
        get { return dataCriacao;}
    }
    public int Counter{
        get { return counter;}
    }

    public void Depositar(decimal val) {
        saldo += val;
        counter += 1;
        listaSaldo.Add(saldo);
    }

    public void Sacar(decimal val) {
        if (saldo < val){
            throw new ArgumentOutOfRangeException("Saldo insuficiente.");
        }
        saldo -= val;
        counter += 1;
        listaSaldo.Add(saldo);
    }

    public decimal SaldoMedio() {
        decimal media = listaSaldo.Sum() / counter;
        return media;
    }

}
