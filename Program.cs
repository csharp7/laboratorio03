﻿using System;

namespace Laboratorio3
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente minhaConta = new ContaCorrente("Maria", 300);
            Console.WriteLine(minhaConta.Saldo);
            minhaConta.Depositar(100);
            Console.WriteLine(minhaConta.Saldo);
            minhaConta.Sacar(50);
            Console.WriteLine(minhaConta.Saldo);
            minhaConta.Depositar(120);
            Console.WriteLine(minhaConta.Saldo);
            minhaConta.Depositar(150);
            Console.WriteLine(minhaConta.Saldo);
            minhaConta.Sacar(200);
            
            Console.WriteLine($"nome do(a) titular: {minhaConta.Titular}");
            Console.WriteLine($"data de criação da conta: {minhaConta.DataCriacao:d}");

            Console.WriteLine($"saldo atual: {minhaConta.Saldo:C}");
            Console.WriteLine($"saldo médio: {minhaConta.SaldoMedio():C}");

        }
    }
}
